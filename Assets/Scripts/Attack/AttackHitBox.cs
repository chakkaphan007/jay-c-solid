using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackHitBox : MonoBehaviour
{
    public AttackController _attackController;
    public GameObject Host { get; set; }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (isActiveAndEnabled)
        {
            Debug.Log("Attack");
            if (col.gameObject == Host) return;
        
            if (col.gameObject.TryGetComponent<Enemy>(out var _Enemy))
            {
                Debug.Log("hit enemy");
                _Enemy.DecreaseHp(_attackController.currentWeapon.Damage);
            }
        }
    }
}
