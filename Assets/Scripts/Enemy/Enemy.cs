using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : EntityBase, IGiveScore
{
    [SerializeField] private int score = 1;

    public override void Death(GameObject _source)
    {
        gameObject.SetActive(false);
        
        if (GameplayManager.Instance.TryGetManager<RespawnManager>(out var _respawnManager))
        {
            _respawnManager.StartRespawn(gameObject);
            currentHp = MaxHp;
        }
        
        if (_source.TryGetComponent<Player>(out _))
        {
            AddScore(score);
        }
    }
    
    public void AddScore(int _score)
    {
        ScoreManager.Instance.AddScore(_score);
    }
}
