using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EntityBase : MonoBehaviour, IDamageable
{
    [SerializeField] protected int MaxHp = 10;
    [SerializeField] protected int currentHp;
    public UnityEvent<int> onHpChanged;

    private void OnEnable()
    {
        currentHp = MaxHp;
    }

    public void ApplyDamage(GameObject _source, int _damage)
    {
        currentHp -= _damage;
        if (currentHp <= 0)
        {
            Death(_source);
        }
    }

    public virtual void Death(GameObject _source)
    {
        Destroy(gameObject);
    }
    
    public void Heal(int _value)
    {
        currentHp += _value;
        onHpChanged?.Invoke(currentHp);
    }
    
    public void DecreaseHp(int _value)
    {
        currentHp -= _value;
        
        if (currentHp <= 0)
        {
            Death(gameObject);
        }
        onHpChanged?.Invoke(currentHp);
    }
}
