using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class Player : EntityBase
{
    public PlayerController _PlayerController;
    public AttackController _AttackController;

    [SerializeField] private Transform spawnPoint;

    private void Start()
    {
        onHpChanged?.Invoke(currentHp);
    }
    
    public void OnMove(InputAction.CallbackContext _context)
    {
        _PlayerController.moveInput = _context.ReadValue<Vector2>();
    }

    public void OnJump(InputAction.CallbackContext _context)
    {
        if (!_PlayerController.isGrounded) return;
        
        _PlayerController.rb.AddForce(Vector2.up * _PlayerController.jumpForce, ForceMode2D.Impulse);
        _PlayerController.isGrounded = false;
    }
    private void FixedUpdate()
    {
        //UpdateMovement
        _PlayerController.rb.velocity = new Vector2
            (_PlayerController.moveInput.x * _PlayerController.moveSpeed, _PlayerController.rb.velocity.y);
        
        // Flip the player sprite when changing direction
        if (_PlayerController.moveInput.x != 0)
        {
            _PlayerController.body.localScale = new Vector3(Mathf.Sign(_PlayerController.moveInput.x), 1f, 1f);
            float _rotate = Mathf.Sign(_PlayerController.moveInput.x) > 0 ? 0 : 180f;
            _AttackController.firePoint.rotation = Quaternion.Euler(0, 0, _rotate);
        }
        
        //CheckGround
        RaycastHit2D _hit = Physics2D.Raycast(transform.position, Vector2.down, 
            _PlayerController.checkGroundRayLenght, _PlayerController.groundLayer);
        
        _PlayerController.isGrounded = _hit.collider != null;
    }
    private void Respawn()
    {
        _PlayerController.rb.velocity = Vector2.zero;
        transform.position = spawnPoint.position;
    }
}
