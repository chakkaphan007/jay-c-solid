using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField] public Rigidbody2D rb;
    [SerializeField] internal Transform body;
    [SerializeField] internal float moveSpeed = 5f;
    [SerializeField] internal float jumpForce = 7f;
    [SerializeField] internal LayerMask groundLayer;
    internal Vector2 moveInput;
    internal bool isGrounded;
    
    #region Movement



    internal readonly float checkGroundRayLenght = 0.6f;

    private void OnDrawGizmos()
    {
        Debug.DrawRay(transform.position, Vector3.down * checkGroundRayLenght, Color.green);
    }

    #endregion
}
